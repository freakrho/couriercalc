use super::package::Purchase;

const WEIGHTS: &'static [f32] = &[
    9.9,
    19.9,
    29.9,
];
const FRACTIONS: &'static[f32] = &[
    1.0,
    0.8,
    0.7,
];
const PRICE_PER_100G: f32 = 2.5;
const UNDER_400_MIN_PRICE: f32= 10.0;
const HANDLING: f32 = 6.0;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, _packages) = pkg.total();
    let mut price = HANDLING;
    if weight < 0.4 {
        price += UNDER_400_MIN_PRICE;
    }
    for i in 0..WEIGHTS.len() {
        if weight <= WEIGHTS[i] {
            return price + (weight / 0.1).ceil() * PRICE_PER_100G * FRACTIONS[i];
        }
    }
    price
}
