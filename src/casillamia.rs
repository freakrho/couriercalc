use super::package::Purchase;

const WEIGHTS: &'static [f32] = &[
    5.0,    
    20.0,
    999.0,
];
const PRICE_PER_500G: &'static[f32] = &[
    10.0,
    4.0,
    6.0,
];
const WEIGHT_STEP: f32 = 0.5;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, _packages) = pkg.total();
    
    let mut price = 0.0;
    for i in 0..PRICE_PER_500G.len() {
        let tier = WEIGHTS[i];
        price += (tier.min(weight) / WEIGHT_STEP).ceil() * PRICE_PER_500G[i];
        if weight <= tier { break; }
    }
    price
}
