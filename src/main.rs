mod puntomio;
mod miamibox;
mod tiendamia;
mod urubox;
mod gripper;
mod casillamia;
mod starbox;
mod package;

use std::{path::PathBuf, fs::File, io::BufReader};

use package::Purchase;

fn main() {
    let args = std::env::args();
    
    if args.len() > 1 {
        let data_file_path = PathBuf::from(&args.collect::<Vec<String>>()[1]);

        let data_file = match File::open(&data_file_path) {
            Ok(file) => file,
            _ => panic!("No file found"),
        };
        let data_reader = BufReader::new(&data_file);
        let purchase: Purchase = match serde_yaml::from_reader(data_reader) {
            Ok(yaml) => yaml,
            _ => Purchase { packages: vec![] },
        };

        let (weight, price, amount, packages) = purchase.total();
        println!("[COMPRA] {} productos, {} paquetes, {}kg, ${}", amount, packages, weight, price);
        println!(" * Punto Mio: {:?} (no confiable, puntomio no es transparente con sus tarifas (que subieron recientemente))", puntomio::price(&purchase));
        println!(" * Miami Box: {:?}", miamibox::price(&purchase));
        println!(" * Tiendamia: {:?}", tiendamia::price(&purchase));
        println!(" * Urubox: {:?}", urubox::price(&purchase));
        println!(" * Gripper: {:?}", gripper::price(&purchase));
        println!(" * Casillamia: {:?}", casillamia::price(&purchase));
        println!(" * Starbox: {:?}", starbox::price(&purchase));
    }
}
