use super::package::Purchase;

static WEIGHTS: &'static [f32] = &[ 0.2,     0.5,   0.7,    1.0];
const PRICES: &'static [f32] = &[   10.90,   15.90, 18.90,  20.90];
static WEIGHTS_PER_KG: &'static [f32] = &[ 5.0,     10.0,   20.0,   40.0];
const PRICES_PER_KG: &'static [f32] = &[   17.90,   17.90,  16.50,  15.90];
const DELIVERY_COST: f32 = 6.15;  // $5 + IVA
const HANDLING: f32 = 5.0;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, _packages) = pkg.total();
    let mut total = DELIVERY_COST + HANDLING;
    for i in 0..WEIGHTS.len() {
        if weight < WEIGHTS[i] {
            total += PRICES[i];
            return total;
        }
    }
    for i in 0..WEIGHTS_PER_KG.len() {
        if weight < WEIGHTS_PER_KG[i] {
            total += PRICES_PER_KG[i] * weight;
            return total;
        }
    }

    total
}
