use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Product {
    #[serde(default = "default_name")]
    pub name: String,
    #[serde(default = "default_value")]
    pub price: f32,
    pub weight: f32,
    pub amount: u16,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Package {
    #[serde(default = "default_name")]
    pub name: String,
    pub products: Vec<Product>,
}

impl Package {
    pub fn total(&self) -> (f32, f32, u16) {
        let mut weight: f32 = 0.0;
        let mut price: f32 = 0.0;
        let mut amount: u16 = 0;
        for p in &self.products {
            weight += p.weight;
            price += p.price;
            amount += p.amount;
        }
        
        (weight, price, amount)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Purchase {
    pub packages: Vec<Package>,
}

impl Purchase {
    pub fn total(&self) -> (f32, f32, u16, usize) {
        let mut weight: f32 = 0.0;
        let mut price: f32 = 0.0;
        let mut amount: u16 = 0;
        for p in &self.packages {
            let (w, p, a) = p.total();
            weight += w;
            price += p;
            amount += a;
        }
        
        (weight, price, amount, self.packages.len())
    }
}

fn default_name() -> String {
    String::new()
}

fn default_value() -> f32 { 0.0_f32 }
