use super::package::Purchase;

const PRICE_PER_KG: f32 = 21.99;
const DISCOUNT_BETWEEN_3_1_AND_5: f32 = 0.3;
const TAX: f32 = 0.095;
const HANDLING_PER_PRODUCT: f32 = 2.99;
const MAX_PRODUCTS_PER_HANDLING: u16 = 3;
const DELIVERY_COST: f32 = 4.99;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, price, amount, _packages) = pkg.total();
    
    // flat rate
    let mut shipping_price: f32 = DELIVERY_COST;
    
    // weight
    let mut discount: f32 = 0.0;
    if weight > 3.0 {
        discount = DISCOUNT_BETWEEN_3_1_AND_5;
    }
    shipping_price += PRICE_PER_KG * (1.0 - discount) * weight;

    // tax
    shipping_price += price * TAX;
    
    // handling
    shipping_price += MAX_PRODUCTS_PER_HANDLING.min(amount) as f32 * HANDLING_PER_PRODUCT;

    shipping_price
}
