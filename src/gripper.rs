use super::package::Purchase;

const WEIGHTS: &'static [f32] = &[ 0.9  ];
const PRICES: &'static [f32] = &[   19.8 ];
const WEIGHTS_PER_KG: &'static [f32] = &[ 5.0,     20.0,   40.0 ];
const PRICES_PER_KG: &'static [f32] = &[   21.90,   16.5,  13.2  ];
const HANDLING: f32 = 5.0;
const MAX_PACKAGES: usize = 5;
const HANDLING_PER_PACKAGE: f32 = 1.0;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, packages) = pkg.total();
    let mut total = HANDLING;
    if packages > MAX_PACKAGES {
        total += (MAX_PACKAGES - packages) as f32 * HANDLING_PER_PACKAGE;
    }
    for i in 0..WEIGHTS.len() {
        if weight < WEIGHTS[i] {
            total += PRICES[i];
            return total;
        }
    }
    for i in 0..WEIGHTS_PER_KG.len() {
        if weight < WEIGHTS_PER_KG[i] {
            total += PRICES_PER_KG[i] * weight;
            return total;
        }
    }

    total
}
