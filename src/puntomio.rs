use super::package::Purchase;

const WEIGHTS: &'static [f32] = &[
    0.5,
    1.0,
];
const PRICES: &'static [f32] = &[
    8.99,
    13.0,
];
const WEIGHTS_PER_KG: &'static [f32] = &[
    1.5,
    2.0,
    2.5,
    3.0,
    3.5,
    4.0,
    4.5,
];
const PRICES_PER_KG: &'static [f32] = &[
    27.0,
    34.0,
    41.0,
    48.0,
    55.0,
    62.0,
    69.0,
];
const HANDLING: f32 = 5.0;
const MAX_PACKAGES: usize = 4;
const HANDLING_PER_PACKAGE: f32 = 2.0;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, packages) = pkg.total();
    let mut total = HANDLING;
    if packages > MAX_PACKAGES {
        total += (MAX_PACKAGES - packages) as f32 * HANDLING_PER_PACKAGE;
    }
    for i in 0..WEIGHTS.len() {
        if weight <= WEIGHTS[i] {
            total += PRICES[i];
            return total;
        }
    }

    for i in 0..WEIGHTS_PER_KG.len() {
        if weight <= WEIGHTS_PER_KG[i] {
            total += PRICES_PER_KG[i] * weight;
            return total;
        }
    }


    total
}
