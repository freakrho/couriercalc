use super::package::Purchase;

static WEIGHTS: &'static [f32] = &[
    0.5,
    0.999,
];
const PRICES: &'static [f32] = &[
    17.0,
    21.0
];
static WEIGHTS_PER_KG: &'static [f32] = &[
    4.99,
    10.0,
];
const PRICES_PER_KG: &'static [f32] = &[
    21.0,
    20.0,
];
const HANDLING_AND_SHIPPING: f32 = 6.15;  // $5 + IVA
const DELIVERY_MVD: f32 = 5.0;

pub fn price(pkg: &Purchase) -> f32 {
    let (weight, _price, _amount, _packages) = pkg.total();
    let mut total = HANDLING_AND_SHIPPING + DELIVERY_MVD;
    for i in 0..WEIGHTS.len() {
        if weight < WEIGHTS[i] {
            total += PRICES[i];
            return total;
        }
    }
    
    for i in 0..WEIGHTS_PER_KG.len() {
        if weight < WEIGHTS_PER_KG[i] {
            total += PRICES_PER_KG[i] * (weight / 0.1).ceil() * 0.1;
            return total;
        }
    }

    total
}
